const db = require("../db/pg.js");

class ThingDao {
    async getAll() {
        const phonesData = await db.query(`SELECT * FROM thing`);
        return phonesData.rows;
    }

    async create(thing) {
        const creationData = await db.query(`INSERT INTO thing(name, status, finish_date) VALUES('${thing.name}', '${thing.status}', '${thing.finish_date}')`);
        return creationData;
    }

    async update(thing) {
        const updateData = await db.query(`UPDATE thing SET name = '${thing.name}', status = '${thing.status}', finish_date = '${thing.finish_date}'`);
        return updateData;
    }

    async delete(id) {
        const deletionData = await db.query(`DELETE FROM thing WHERE id = ${id}`);
        return deletionData;
    }
}

module.exports = ThingDao;