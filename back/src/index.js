const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const ThingDao = require('./dao/ThingDao');
const thingDao = new ThingDao();

var app = express();
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', async function (req, res) {
    const things = await thingDao.getAll();
    res.send(things);
})

app.post('/', async function (req, res) {
    const thing = req.body;
    try {
        await thingDao.create(thing);
        res.json({ status: 'ok' })
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
})

app.put('/', async function (req, res) {
    const thing = req.body;
    try {
        await thingDao.update(thing);
        res.json({ status: 'ok' })
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
})

app.delete('/:id', async function (req, res) {
    const id = req.params.id;
    try {
        await thingDao.delete(id);
        res.json({ status: 'ok' })
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
})

app.listen(3000, () => console.info("Listening port 3000"));