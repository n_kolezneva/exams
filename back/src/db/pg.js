const { Client } = require("pg");

const client = new Client({
    connectionString: 'postgres://postgres:postgres@localhost:5432/bmstu'
});

client.connect();

module.exports = client;