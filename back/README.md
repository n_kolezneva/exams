# Node.js backend

## Before running

### First step - PostgreSQL

1. In terminal `psql postgres`  
2. `CREATE DATABASE bmstu`  
3. `\connect bmstu`  
4. Create table with script from `script.sql`  

### Second step - npm

1. `npm install`  

### Third step - run

1. `node src/index.js`  