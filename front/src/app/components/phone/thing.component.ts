import { ThingService } from '../../service/thing.service';
import { Thing } from '../../model/Thing';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-thing',
  templateUrl: './thing.component.html',
  styleUrls: ['./thing.component.css']
})
export class ThingComponent {
  @Input() thing: Thing;
  @Output() deleteThingFromList: EventEmitter<any> = new EventEmitter();
  nowdate='2020-01-18';
  isEditting = false;

  constructor(public thingService: ThingService) { }

  onEdit() {
    if (this.isEditting) {
      this.thingService.editThing(this.thing);
    }
    this.isEditting = !this.isEditting;
  }

  onDelete() {
    console.log(this.thing);
    this.thingService.deleteThing(this.thing);
    this.deleteThingFromList.emit();
  }
}
